# Overview

### Important notes

* All endpoints start with /probe/{version}, in case of version 1 is /probe/v1
* To use our API successfully you'll need an api-key provided by us, if you don't have it, please request one
* If the client beeing tested has more than one host, all of them will be tested, respecting the condition that if at least one is up, the connection is ok
* All IPs used on examples will be symbolically inspired on localhost (127.0.0.1) for security reasons
* Each connection has 5 seconds of timeout, so if a client has 3 hosts and all of them are off, the response will last 3 x 5 seconds (15 seconds) 

#### Checking Connection

This endpoint is used to check our connection with a specific client. To be able to do this, the destination must be previously configured.

> GET /source_name/env/destination_name

* **source_name**: configured name of the partner that is using the API to verify connections
* **env**: environment to test the connection
    * **hml**: for tests
    * **prd**: for production
* **destination_name**: configured name of the partner that the connection will be tested