# /vero

> All endpoints from vero

### GET /prd/dxc

> Check connection with DXC (Authorizer) - Production

#### Request

```
/probe/v1/vero/prd/dxc
```

#### Response

##### Online - HTTP 200

```json
{
    "server": "online",
    "connections": [
        {
            "server": "online",
            "ip": "127.0.0.1",
            "port": 80
        },
        {
            "server": "offline",
            "ip": "127.0.0.2",
            "port": 80
        },
        {
            "server": "offline",
            "ip": "127.0.0.3",
            "port": 80
        }
    ]
}
```

##### Offline - HTTP 504

```json
{
    "server": "offline",
    "connections": [
        {
            "server": "offline",
            "ip": "127.0.0.1",
            "port": 80
        },
        {
            "server": "offline",
            "ip": "127.0.0.2",
            "port": 80
        },
        {
            "server": "offline",
            "ip": "127.0.0.3",
            "port": 80
        }
    ]
}
```
---

### GET /hml/dxc

> Check connection with DXC (Authorizer) - Test

#### Request

```
/probe/v1/vero/hml/dxc
```

#### Response

##### Online - HTTP 200

```json
{
    "server": "online",
    "ip": "127.0.0.1",
    "port": 80
}
```

##### Offline - HTTP 504

```json
{
    "server": "offline",
    "ip": "127.0.0.1",
    "port": 80
}
```
---

### GET /<hml|prd>/paysmart

> Check connection with paySmart Gateway

#### Request

```
/probe/v1/vero/hml/paysmart
/probe/v1/vero/prd/paysmart
```

#### Response

##### Online - HTTP 200

```json
{
    "server": "online",
    "ip": "127.0.0.1",
    "port": 80
}
```

##### Offline - HTTP 504

```json
{
    "server": "offline",
    "ip": "127.0.0.1",
    "port": 80
}
```

---